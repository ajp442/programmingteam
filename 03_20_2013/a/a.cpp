#include <fstream>
#include <string>
#include <cmath>
#include <iomanip>

using namespace std;

typedef long long LL;
typedef long double LD;

int main(int argc, char** argv)
{
	ifstream fin("a.in");
	ofstream fout("a.out");
	LD hr, min;
	LD hr_ang, min_ang;
	LD ang;

	while(fin >> hr >> min)
	{
		if(hr == 0 && min == 0)
			break;

		min_ang = 360.0 * min / 60.0;
		hr_ang = 360.0 * hr / 12.0;

		if(hr_ang == 360)
			hr_ang = 0;

		hr_ang += (360.0 / 12.0) * min_ang / 360.0;

		if(min_ang > hr_ang)
			ang = min_ang - hr_ang;
		else
			ang = hr_ang - min_ang;

		if(ang > 180)
			ang = 360 - ang;

		fout << "At ";

		if((LL)hr > 0)
			fout << (LL)hr;
		else
			fout << 12;

		fout << ":";

		if((LL)min < 10)
			fout << 0;

		fout << (LL)min;
		fout << " the angle is " << fixed << setprecision(1) << ang;
		fout << " degrees." << endl;
	}

	return 0;
}
