#include <fstream>
#include <string>
#include <cmath>
#include <iomanip>

using namespace std;

typedef long long LL;
typedef long double LD;

int main(int argc, char** argv)
{
	ifstream fin("b.in");
	ofstream fout("b.out");

	LL a[100000] = {0};
	LL i, j, k, m;
	i = 1;
	j = 3;
	a[0] = 2;
	bool prime;

	while( j < 10000 )
	{
		m = sqrt(j);
		prime = true;
		for( k = 3; k <= m; k += 2 )
		{
			if( j % k == 0 )
			{
				prime = false;
				break;
			}
		}
		if(prime)
		{
			a[i] = j;
			i++;
		}
		j += 2;	
	}	

	LL n;
	int result;

	int count;
	count = 0;

	while(fin >> n)
	{
		if( n == -1 )
		{
			return 0;
		}
		if( count > 0)
			fout << endl;
		fout << "M(" << n << ") = ";
		result = 1;
		if( n != 1 )
		{
			i = 0;
			k = 0;
			while( a[i] != 0 )
			{
				if( n % a[i] == 0 )
				{
					n /= a[i];
					k++;
					if(n % a[i] == 0)
					{
						result = 0;
						break;
					}
				}
				i++;
			}
			if( result != 0 )
			{
				if( k % 2 == 0 )
				{
					result = 1;
				}else
					result = -1;

			}
		}
		fout << result << endl;
		count++;
	}
	return 0;
}
