#include <fstream>
#include <algorithm>
#include <sstream>
#include <string>
#include <cmath>
#include <iomanip>
#include <vector>

using namespace std;

typedef long long LL;
typedef long double LD;

LL maxCount;

LL findLinks(LL i, LL numbers);
bool comp(string a, string b);
void traceList(vector<string>& numbers, LL current, LL count);

int main(int argc, char** argv)
{
	ifstream fin("c.in");
	ofstream fout("c.out");
	vector<string> numbers;
	numbers.reserve(100000);
	LL nums;
	LL temp;
	stringstream ss;
	LL caseCount = 1;

	
	while(fin >> nums)
	{
		if (nums == 0)
			break;
		if (caseCount != 1)
			fout << endl;
		
		for (LL i = 0; i<nums; i++)
		{
			fin >> temp;
			ss << temp;
			numbers.push_back(ss.str());
			ss.str("");

			if(numbers[i].length() < 5)
				numbers[i].insert((string::size_type)0, 5 - numbers[i].length(), '0');
		}
		

		sort(numbers.begin(), numbers.end());
		
		maxCount = 0;
		for(LL j = 0; j < numbers.size(); j++)
			traceList(numbers, j, 1);
		fout << "Case " << caseCount << ". " << maxCount << " values" << endl;
		caseCount++;
		numbers.clear();
	
	}

	return 0;
}

void traceList(vector<string>& numbers, LL current, LL count)
{
	int i = 1;
	while(current + i < numbers.size())
	{
		if(comp(numbers[current], numbers[current+i]))
		{
			traceList(numbers, current+i, count + 1);
		}
		i++;
	}
	if (count > maxCount){
		maxCount = count;
	}
	
	return;

	
}
bool comp(string a, string b)
{
	int count = 0;
	for(int i = 4; i>=0; i--)
	{
		if ( (b[i]-a[i])!=0)
		{
			count ++;
			if (count > 1)
				return false;
		}
	}
	if (count == 0)
		return false;
	return true;
}


