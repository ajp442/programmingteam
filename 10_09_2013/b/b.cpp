#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

using namespace std;

struct entity
{
	bool rights[26] = {false};
};

char ops[] = "+=-";

//entity * list[26] = {NULL};

void plus1(char* ents, char* rights, entity * list[26]);
void minus1(char* ents, char* rights, entity * list[26]);
void equals1(char* ents, char* rights, entity * list[26]);


int main()
{
	
	ifstream fin;
	ofstream fout;
	char log[100];
	char* tokptr;
	char* op;
	bool temp[26] = {false};
	bool change = true;
	entity * list[26] = {NULL};

	fin.open("b.in");
	fout.open("b.out");

	while(fin >> log)
	{
		if(log[0] == '#')
			break;
		tokptr = strtok( log, ",");
		while(tokptr != NULL)
		{
			op = strpbrk(tokptr, ops);
			switch (op[0])
			{
				case '+':
					plus1(tokptr, op+1, list);
					break;
				case '-':
					minus1(tokptr, op+1, list);
					break;
				case '=':
					equals1(tokptr, op+1, list);
					break;
			}
			tokptr = strtok( NULL, ",");
		}
		for(int i = 0; i < 26; i++)
		{
			
			if(list[i] != NULL)
			{	
				if(change)
					for(int j = 0; j < 26; j++)
							temp[j] = list[i]->rights[j];
				fout << char(i+65);
				change == false;
				for(int j = 0; j < 26; j++)
				{
					if(temp[j] != list[i]->rights[j])
					{
						change == true;
						break;
					}
				}
				if(change)
				{
					for(int j = 0; j < 26; j++)
						if(temp[j])
							fout << char(j+97);
					
				}
				
				
								
				
			}	
				
		}
		fout << endl;
		for(int i = 0; i < 26; i++)
		{	
			if(list[i] != NULL)
				delete list[i];
			temp[i] = false;
			change = true;
		}
		
		
	}
	return 0;
}

void plus1(char* ents, char* rights, entity * list[26])
{
	for(int i = 0; ents[i] != '+'; i++)
	{	
		if(list[ents[i]-65] == NULL)
			list[ents[i]-65] = new (nothrow) entity;
		for(int j = 0; rights[j] != '\0'; j++)
			list[ents[i]-65]->rights[rights[j]-97] = true;
	}
	
}
void minus1(char* ents, char* rights, entity * list[26])
{
	for(int i = 0; ents[i] != '-'; i++)
	{	
		if(list[ents[i]-65] == NULL)
			list[ents[i]-65] = new (nothrow) entity;
		for(int j = 0; rights[j] != '\0'; j++)
			list[ents[i]-65]->rights[rights[j]-97] = false;
	}
	
}
void equals1(char* ents, char* rights, entity * list[26])
{
	for(int i = 0; ents[i] != '='; i++)
	{	
		delete list[ents[i]-65];
		list[ents[i]-65] = new (nothrow) entity;
		for(int j = 0; rights[j] != '\0'; j++)
			list[ents[i]-65]->rights[rights[j]-97] = true;
	}
	
}

