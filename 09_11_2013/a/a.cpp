// Team Awesome: Andrew Pierson, Zachary Pierson
// Location: opp lab
// date: 9-11-2013


#include <iostream>
#include <fstream>

#define LL long long int

using namespace std;

LL findgcd (LL a, LL b);

int main() 
{
    LL lcm;
    LL gcd;
    LL a;
    LL b;
   ifstream fin;
   ofstream fout;
   fout.open("a.out");
   fin.open("a.in");
   LL num;
   fin >> num;

   for(LL i=1; i<=num; i++)
   {
     fin >> a;
     fin >> b;

     gcd = findgcd(a, b);
     lcm = (a*b)/gcd;

     fout << i << " " << lcm << " " << gcd << endl;
   }

}

LL findgcd (LL a, LL b)
{
    if (b == 0)
        return a;
    return findgcd(b, a%b);
}

