
// Team Awesome: Andrew Pierson, Zachary Pierson
// Location: opp lab
// date: 9-11-2013


#include <iostream>
#include <fstream>
#include <climits>
#include <limits.h>

#define LL unsigned long long int

using namespace std;
LL dyntbl[429496729] = {0};

LL pal(int n);

LL findgcd (LL a, LL b);

int main() 
{

   ifstream fin;
   ofstream fout;
   fout.open("d.out");
   fin.open("d.in");
   LL num;
   fin >> num;
   LL N;
   LL res;

   for(LL i=1; i<=num; i++)
   {
        fin >> N;
        res = pal(N);
        fout << i << " " << res << endl;
   }
}

LL pal(int n)
{
    if (n == 1)
    {
        return 1;
    }
    if (dyntbl[n] != 0)
        return dyntbl[n];

    LL res = 0;
    for(LL i = 1; i<=n/2; i++)
    {
        res += pal(i);
    }

    dyntbl[n] = res + 1;
    return res + 1;
}
