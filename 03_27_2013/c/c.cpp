#include <iostream>
#include <fstream>

using namespace std;

int isinarray(int in, int incoming[])
{
    for(int i = 0; i < 6; i++)
        if(incoming[i] == in)
            return i;
    return -1;
}

int arrayempty(int array[])
{
    for(int i = 0; i < 5; i++)
        if(array[i] != 0)
            return false;
    return true;
}

void divideup(int incoming, int curr[], int counts[], bool dup=true)
{
    int temp = incoming;
    int skip;
    if(!dup)
        skip = isinarray(incoming, curr);
        
    for(int i = 0; i < 6; i++)
    {
        counts[i] = 0;
        if(!dup && i == skip)
            continue;
        int d = temp / curr[i];
        temp -= curr[i] * d;
        counts[i] = d;
    }
}

void addarr(int to[], int from[])
{
    for(int i = 0; i < 6; i++)
    {
        //cout << i << " -> " << to[i] << " + " << from[i] << endl;
        to[i] += from[i];
    }
}

int main()
{
    int incoming, d, r, i, q, entirecount = 0;
    int curr[6] = {500,100,25,10,5,1};
    int counts[6] = {0};
    int tempcounts[6] = {0};
    ifstream in("c.in");
    ofstream out("c.out");
    
    while(in >> incoming)
    {
        divideup(incoming, curr, counts);
        entirecount = 1;
        bool set = false;
        for(q = 4; q >= 0;)
        {
            cout << q << " = " << endl;
            if(counts[q] != 0)
            {
                counts[q]--;
                divideup(curr[q], curr, tempcounts, false);
                addarr(counts, tempcounts);
                for(int i = 0; i < 6; i++)
                {
                    cout << counts[i] << " ";
                }
                cout << endl;
                entirecount++;

                q = 4;
            }
            else
                q --;
        }
        cout << entirecount << endl;
    }
    
    return 0;
}
