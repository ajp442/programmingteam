//Andrew Pierson
//Opp Lab
//09-18-2013
//
#include <iostream>
#include <fstream>

#define LL long long int
//#define LEN 967295
#define LEN 4294967294

using namespace std;


int main()
{
    ifstream fin;
    ofstream fout;
    fin.open("a.in");
    fout.open("a.out");
    LL first = 0;
    LL last = 1;
    LL diff = 2;
    LL caseNum = 0;

    //bool set[LEN] = {0};
    //    bool *set;
    //    set = new bool[LEN];
    //
    //    bool *negset;
    //    negset = new bool[LEN];
    //


    while (first <= last)
    {
        LL x = 0;
        caseNum++;
        fin >> first;
        fin >> last;
        fin >> diff;

        if (diff < 0)
        {
            diff = -diff;
        }

        LL upcurr = first;
        LL downcurr = last;
        LL lastupcurr;
        LL lastdowncurr;
        if (diff == 0 && first < last)
        {
            fout << "Case " << caseNum << ": Set contains " << 2 << " integers." << endl;
        }
        else if (diff == 0 && first == last)
        {
            fout << "Case " << caseNum << ": Set contains " << 1 << " integers." << endl;
        }
        else
        {
            while (downcurr > first && downcurr != upcurr && lastupcurr != downcurr)
            {

                x++;
                x++;
                lastdowncurr = downcurr;
                lastupcurr = upcurr;
                upcurr += diff;
                downcurr -= diff;

            }
            if (upcurr == downcurr)
                x++;

            if (last >= first)
                fout << "Case " << caseNum << ": Set contains " << x << " integers." << endl;
        }
    }


}
