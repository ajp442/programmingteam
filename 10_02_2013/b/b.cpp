#include <iostream>
#include <fstream>
#include <bitset>
#include <cmath>

using namespace std;

typedef long long LL;

int main()
{

	LL n;

	ifstream fin;
	fin.open("b.in");
	
	ofstream fout;
	fout.open("b.out");

	LL codes[500] = {0};
	LL min_dist = 6000000000;
	LL count = 0;

	while ( fin >> n)
	{
		count ++;
		min_dist = 6000000000;
		if ( n == 0)
			break;
	
		for (LL i = 0; i<n; i++)
		{
			fin >> codes[i];
		}

		for (LL i = 0; i<n; i++)
		{
			bitset<32> a(codes[i]);
			for (LL j = i+1; j<n; j++)
			{
				LL dist = 0;
				bitset<32> b(*(unsigned long long *)&codes[j]);
				for (LL k = 0; k<32; k++)
				{
					if(a[k] != b[k])
					{
						dist++;
					}
				}
				if (dist < min_dist)
					min_dist = dist;
			}
		}
		fout << "Case " << count << ". Hamming distance = " << min_dist << "." << endl;
					
			
	}

	fin.close();
	fout.close();

}
