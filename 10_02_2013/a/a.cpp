//Red Team, 306

#include <fstream>
#include <iostream>
#include <iomanip>

using namespace std;

const double e = 0.00000001;

int main()
{
        ifstream fin("a.in");
        ofstream fout("a.out");
        long double total2 = 0;
        long double total = 0;
        long double intrest = 0;
        long double rate = 0;
        bool keepGoing = true;
        long long n = 0;
        while(fin >> total >> intrest >> rate && keepGoing){
        n++;
        total2 = total;
          if(total2 != 0 || intrest != 0 || rate != 0){
             long double annualRate = intrest / rate + e;
             annualRate /= 100.0;
             annualRate += e;
             for(long long counter = 0; counter < rate; counter++){
              long double monthsIntrest = total * annualRate + e;
              
              long double hi = monthsIntrest * 100.0 + e;
              long long t = hi;
              monthsIntrest = t / 100.0;
              
              total += monthsIntrest + e;
               
             }
            fout << "Case " << n << setprecision(2) << fixed << showpoint << ". $" << total2 << " at " << intrest << "\% APR compounded ";
            fout << (long long)rate << " times yields $" << setprecision(2) << fixed << showpoint << total << "\n";
          }else{
            keepGoing = false;
          }
         
          
        }
       
        
        fin.close();
        fout.close();
        
        return 0;
}
