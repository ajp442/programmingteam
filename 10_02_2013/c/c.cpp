#include <fstream>
#include <cstring>
#include <string>
#include <vector>
#include <set>
#include <algorithm>
#include <numeric>

using namespace std;

typedef long long ll;

int main()
{
        ifstream fin("c.in");
        ofstream fout("c.out");
        char num[1000000] = { 0 };
        string snum;
        
        while(fin.getline(num, 999999))
        {
                set<string> s;
                snum = num;
                
                sort(snum.begin(), snum.end());
                
                s.insert(snum);
                while(next_permutation(snum.begin(), snum.end()))
                        s.insert(snum);
                
                for(auto &x : s)
                        if(x[0] != '0')
                                fout << x << endl;
                                
                fout << "----------" << endl;
        }
        
        fin.close();
        fout.close();
        
        return 0;
}
